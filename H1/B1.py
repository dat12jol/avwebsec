#!/usr/bin/python3
import sys
def lunh(line):
    tmp_sum = 0
    lst = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
    for i in range( len(line)):
        if line[i].isdigit():
            if i % 2 == 0:
                tmp_sum += lst[int(line[i])]
            else:
                tmp_sum += int(line[i])
        else:
            nbr = i
    nbr = (lst.index((10 - (tmp_sum % 10)) % 10)) if nbr % 2 == 0 else ((10 - (tmp_sum % 10)) % 10)
    return nbr
nbr_list = open(str(sys.argv[1])).readlines()
output_sequence = ''
for line in nbr_list:
    line = line.strip()
    cand_nbr = lunh(line)
    if cand_nbr != -1:
        output_sequence = output_sequence + str(cand_nbr)
print(output_sequence)
