#!/usr/bin/python3
import sys
import random

def calculate_confidense(ball_list):
    if len(ball_list) == 1:
        return sum(ball_list)
    mean = sum(ball_list)/len(ball_list)
    variance = 0
    for i in range(len(ball_list)):
        variance = variance + (ball_list[i] - mean) ** 2
    variance = variance/len(ball_list)
    standard_deviation = variance ** (1/2)
    return LAMBDA * (standard_deviation / (len(ball_list) ** (1/2)))

#initialize input variables
bins_exponent = int(sys.argv[1])
required_collisions = int(sys.argv[2])
coins_expected = int(sys.argv[3])
confidense_width = int(sys.argv[4])

conf = float('Inf')
LAMBDA = 3.66 * 2
coins_mined = 0
ball_counter = 0
iteration_counter = 0
bins_total = 2 ** bins_exponent
bins = [[0, 0] for i in range(bins_total)]
iteration_statistics = []

while conf >= confidense_width:
    while coins_mined < coins_expected:
        ball_counter = ball_counter + 1
        rnd = random.randrange(bins_total)
        #refresh the bin if its a new iteration
        if bins[rnd][1] != iteration_counter:
            bins[rnd][0] = 0
            bins[rnd][1] = iteration_counter
        bins[rnd][0] = bins[rnd][0] + 1
        # if the random bins contains exactly the amount
        # needed for a coin it will be regarded as mined
        # if it contains more then it will already be
        # regarded as mined
        if bins[rnd][0] == required_collisions:
            coins_mined = coins_mined + 1
    iteration_statistics.append(ball_counter)
    iteration_counter = iteration_counter + 1
    ball_counter = 0
    coins_mined = 0
    conf = calculate_confidense(iteration_statistics)
print('iterations needed:',  str(iteration_counter), 'mean', str(sum(iteration_statistics)/len(iteration_statistics)))
