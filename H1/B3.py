import sys
import hashlib as hl
import binascii
import math as m

def calculate_merkle_root(merkle_path):
    merkle_node = binascii.unhexlify(merkle_path.pop(0).strip())
    for line in merkle_path:
        line = line.strip()
        if line[0]=='R':
            new_node = hl.sha1(merkle_node + (binascii.unhexlify(line[1:])))
        else:
            new_node = hl.sha1(binascii.unhexlify(line[1:]) + (merkle_node))
        merkle_node = new_node.digest()
    return new_node.hexdigest()
def build_merkle_tree(node_list):
    levels = m.ceil(m.log(len(node_list), 2))
    total_nodes = (2**(levels+1))-1
    merkle_tree = ["empty" for i in range(total_nodes)]
    new_node_list =  []
    for i in reversed(range(1, levels + 1)):
        first_leaf_index = (2 ** i) - 1
        for j in range(len(node_list)):
            merkle_tree[first_leaf_index + j] = node_list[j].strip()
            if ((j % 2 == 0) and ((j + 1) < len(node_list))):
                parent = hl.sha1(binascii.unhexlify(node_list[j].strip()))
                parent.update(binascii.unhexlify(node_list[j + 1].strip()))
                parent = parent.hexdigest()
                merkle_tree[m.floor((first_leaf_index + j - 1)/2)] = parent
                new_node_list.append(parent)
            elif (j % 2 == 0):
                merkle_tree[first_leaf_index + j + 1] = node_list[j].strip()
                parent = hl.sha1(binascii.unhexlify(node_list[j].strip()))
                parent.update(binascii.unhexlify(node_list[j].strip()))
                parent = parent.hexdigest()
                merkle_tree[m.floor((first_leaf_index + j - 1) / 2)] = parent
                new_node_list.append(parent)
        node_list = new_node_list
        new_node_list =  []
    return merkle_tree
def build_merkle_path(leaf_index, depth_index, merkle_tree):
    levels = int(m.log(len(merkle_tree) + 1, 2))
    node_index = (2 ** (levels - 1)) - 1 + int(leaf_index)
    for i in reversed(range(1, levels)):
        if(node_index % 2 == 1):
            tmp_str = 'R' + merkle_tree[node_index + 1]
        else:
            tmp_str = 'L' + merkle_tree[node_index - 1]
        print("level:", i, "sibling:", tmp_str)
        if(i == int(depth_index)):
            result = tmp_str
        node_index = m.floor((node_index - 1) / 2)
    print("merkle root:", merkle_tree[0])
    print("----------------------------------------------------")
    print("result:", result + merkle_tree[0])
    return
print("-------------------------------------------------------")
print("merkle root:", calculate_merkle_root(open(str(sys.argv[1])).readlines()))
print("-------------------------------------------------------")
input_list = open(str(sys.argv[2])).readlines()
merkle_tree = build_merkle_tree(input_list[2:])
build_merkle_path(input_list[0], input_list[1], merkle_tree)
