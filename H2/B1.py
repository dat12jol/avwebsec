import sys
SA, SB, DA, DB, M, b = int(sys.argv[1], 16), int(sys.argv[2], 16), int(sys.argv[3], 16), int(sys.argv[4], 16), int(sys.argv[5], 16), int(sys.argv[6])
if b == 1:
    print(hex(SA ^ SB ^ M)[2:].zfill(4).upper())
else:
    print((hex(SA ^ SB)[2:].zfill(4) + hex(SA ^ SB ^ DA ^ DB)[2:].zfill(4)).upper())
