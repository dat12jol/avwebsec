#!/usr/bin/env python
from pcapfile import savefile
import sys

def ip2int(ipaddress):
    return int(''.join([str(hex(int(a))[2:].zfill(2)) for a in ipaddress.split('.')]), 16)
def disjoint(setlst, r):
    for s in setlst:
        if not s.isdisjoint(r):
            return False
    return True

testcap = open(sys.argv[1], 'rb')
capfile = savefile.load_savefile(testcap, layers=2, verbose=True)
ip_of_abu = sys.argv[2]
ip_of_mix = sys.argv[3]
nbr_of_partners = int(sys.argv[4])
ip_src = []
ip_dst = []
disjoint_list = []
exclusion_list = []

for pkt in capfile.packets:
    ip_src.append(pkt.packet.payload.src.decode('UTF8'))
    ip_dst.append(pkt.packet.payload.dst.decode('UTF8'))
#learning phase
get_batch = collect = False
tmp_set = set()
for index in range(len(ip_src)):
    if ip_src[index] == ip_of_abu:
        get_batch = True
    if ip_src[index] == ip_of_mix and get_batch:
        collect = True
        tmp_set.add(ip_dst[index])
    try:
        if get_batch and collect and ip_src[index + 1] != ip_of_mix :
            if disjoint(disjoint_list, tmp_set) and len(disjoint_list) < nbr_of_partners:
                disjoint_list.append(tmp_set)
            else:
                exclusion_list.append(tmp_set)
            get_batch = collect = False
            tmp_set = set()
    except IndexError:
        if disjoint(disjoint_list, tmp_set) and len(disjoint_list) < nbr_of_partners:
            disjoint_list.append(tmp_set)
        else:
            exclusion_list.append(tmp_set)
        continue
#exclusion phase
for r in exclusion_list:
    i, nbr_of_disjoints = -1, 0
    for ri in disjoint_list:
        if r & ri != set():
            i = disjoint_list.index(ri)
            nbr_of_disjoints += 1
    if nbr_of_disjoints == 1:
        disjoint_list[i] = r & disjoint_list[i]
#calc ip phase
ipsum = 0
for r in disjoint_list:
    if len(r) > 1:
        error("exclusion didn't work")
    ipsum = ipsum + ip2int(r.pop())

print(ipsum)
