import sys
import hashlib as hl
import binascii as b
import matplotlib.pyplot as plt
xlist = []
blist = []
slist = []
m0list = []
m1list = []
v0, v1 = bin(0)[2:], bin(1)[2:]
for K in range(2**16):
    m0list.append(bin(int(hl.sha1(str(v0 + bin(K)[2:].zfill(16)).encode('utf-8')).hexdigest(), 16))[2:])
    m1list.append(bin(int(hl.sha1(str(v1 + bin(K)[2:].zfill(16)).encode('utf-8')).hexdigest(), 16))[2:])
for X in range(1,46):
    v0_set = set()
    v1_set = set()
    binding_lost = False
    second_image = 1
    for K in range(2**16):
        m0 = m0list[K][:X]
        m1 = m1list[K][:X]
        if m0 in v1_set:
            binding_lost = True
            second_image +=1
        if m1 in v0_set:
            binding_lost = True
            second_image +=1
        if m0 in v0_set:
            second_image += 1
        if m1 in v1_set:
            second_image += 1
        v0_set.add(m0)
        v1_set.add(m1)
    print(X, binding_lost, second_image)
    xlist.append(X)
    blist.append(binding_lost)
    slist.append(second_image)

plt.figure()
plt.subplot(211)
plt.plot(xlist,blist)
plt.subplot(212)
plt.plot(xlist, slist)
plt.show()
