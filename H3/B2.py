import numpy as np
from scipy import interpolate as sp

n = 6
my_poly = "15 + 14x + 18x2 + 13x3 +  6x4"

f21 = 38
f31 = 67
f41 = 65
f51 = 67
f61 = 63

poly = lambda x: [int(s.split("x")[0].strip()) for s in x.split("+")[::-1]]
val = np.polyval(poly(my_poly), 1)
for i in range(2, n+1): val += eval(str("f" + str(i) + "1"))
new_poly = sp.lagrange([1, 2, 3, 5, 6], [val,  1963, 7468, 48346, 96691])

print(new_poly(0))
