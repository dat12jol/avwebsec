import requests as rq
import time
import urllib3
import numpy as np

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


name = "Kalle"
grade = "5"
cand = "0123456789abcdef"
url = "https://eitn41.eit.lth.se:3119/ha4/addgrade.php?name=" + name +  "&grade=" + grade + "&signature="

signature = list("XXXXXXXXXXXXXXXXXXXX")
index = 0
while index < 20:
    tmp_str = signature
    for i in cand:
        tmp_str[index] = i
        start = time.time()
        page = rq.get(url + "".join(tmp_str), verify=False)
        end = time.time()
        print("character:", i,"\ttime:",  int(((end - start) * 1000)))
    opt = input("enter char, (back) do go back one step, (again) to run this again")
    if opt == "back":
        index = index -1
        continue
    if opt == "again":
        continue
    signature[index] = opt

print("--------------------")
print("signature:", "".join(signature))
print(rq.get(url + "".join(signature), verify=False).content)
