from hashlib import sha1
from binascii import unhexlify
from binascii import hexlify
import math


def OAEP_encode(M, seed):
    k = 128
    if len(M) > k - 42:
        error("message too long")
    lHash = sha1(unhexlify("")).hexdigest()
    ps = "".zfill((k - len(M)//2 - 42)) * 2
    DB = lHash + ps + '01' + M
    dbMask = MGF1(seed, k-21)
    maskedDB = hex(int(DB, 16) ^ int(dbMask, 16))[2:].zfill((k-21)*2)
    seedMask = MGF1(maskedDB, 20)
    maskedSeed = hex(int(seed, 16) ^ int(seedMask, 16))[2:].zfill(40)
    EM = '00' + maskedSeed + maskedDB
    return EM

def OAEP_decode(EM):
    k = 128
    if len(EM) < 42:
        error("decryption error")
    lHash = sha1(unhexlify("")).hexdigest()
    Y = EM[0:2]
    maskedSeed = EM[2:42]
    maskedDB = EM[42:]
    seedMask = MGF1(maskedDB, 20)
    seed = hex(int(maskedSeed, 16) ^ int(seedMask, 16))[2:].zfill(20)
    dbMask = MGF1(seed, k - 21)
    DB = hex(int(maskedDB, 16) ^ int(dbMask, 16))[2:].zfill((k-21)*2)
    if DB[0:40] != lHash or Y != '00' or DB.find('01') == -1:
        error("decryption error")
    m = DB.split('01')[-1]
    return m

def I2OSP(x, xLen):
    if x >= pow(256, xLen):
        error("integer too large")
    ret = []
    while x:
         ret.append(int(x % 256))
         x = x // 256
    for i in range(xLen - len(ret)):
        ret.append(0)
    return ret[::-1]

def MGF1(mgfSeed, maskLen):
    if maskLen > pow(2,32) * maskLen:
        error("mask too long")
    T = ""
    for counter in range(math.ceil(maskLen / 20)):
        C = I2OSP(counter, 4)
        T += sha1(unhexlify(mgfSeed) + bytes(C)).hexdigest()
    return T[0:maskLen*2]

mgfSeed = "9b4bdfb2c796f1c16d0c0772a5848b67457e87891dbc8214"

maskLen = 21
print(MGF1(mgfSeed, maskLen))

M = "c107782954829b34dc531c14b40e9ea482578f988b719497aa0687"
seed = "1e652ec152d0bfcd65190ffc604c0933d0423381"
EM = "0063b462be5e84d382c86eb6725f70e59cd12c0060f9d3778a18b7aa067f90b2178406fa1e1bf77f03f86629dd5607d11b9961707736c2d16e7c668b367890bc6ef1745396404ba7832b1cdfb0388ef601947fc0aff1fd2dcd279dabde9b10bfc51efc06d40d25f96bd0f4c5d88f32c7d33dbc20f8a528b77f0c16a7b4dcdd8f"

print("encoded message", OAEP_encode(M, seed))
print("decoded message", OAEP_decode(EM))
