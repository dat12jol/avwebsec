import hashlib as hl


def convertBase(n, b):
    if n == 0:
        return [0]
    digits = []
    while n:
        digits.append(int(n % b))
        n //= b
    return digits[::-1]



#  I2OSP converts a nonnegative integer to an octet string of a specified length.
def I2OSP(x,xLen):

    l=convertBase(x,256)
    pre = ''
    for i in range(0,2*(xLen-len(l))):
        pre +='0'
    number=''
    for s in l:
        n=(str(hex(s))[2:])
        if int(n,16)<16:
            n='0'+n
        number+=n
    return pre+number



def MGF1(z,l):
    if l > 2**32:
        print("Too Large Integer")
        return
    hLen=20
    T=''
    counter =0
    for i in range(0,math.ceil(l/hLen)):
        c=I2OSP(i,4)
        sha=hl.new("sha1")
        sha.update(bytes.fromhex(z+c))
        t=sha.hexdigest()
        T+=t
    return T[:l*2]


MGF1('0123456789abcdef',30)


#XoR snodd från stackOverFlow
def change_to_be_hex(str):
    return int(str,base=16)
def xor_two_str(str1,str2):
    a = change_to_be_hex(str1)
    b = change_to_be_hex(str2)
    return hex(a ^ b)[2:]


def OAEP_encode(M,seed):
    k=128
    mLen= len(M)
    hLen=20
    #Step a
    sha=hl.new("sha1")
    sha.update(bytes.fromhex(''))
    lHash=sha.hexdigest()
    #Step b Generate a padding string PS consisting of k - mLen - 2hLen - 2 zero octets.
    PS=''
    for i in range(0,(k-mLen-2*hLen-2)*2):
        PS+='0'

    #Step c Concatenate lHash, PS, a single octet with hexadecimal value 0x01, and the message M
    DB = lHash+PS+'01'+M
    #Step d, seed is already given

    #Step e Let dbMask = MGF(seed, k - hLen - 1).
    dbMask = MGF1(seed,k-hLen-1)

    #Step f Let maskedDB = DB \xor dbMask.
    maskedDB = xor_two_str(DB,dbMask)

    #Step g Let seedMask = MGF(maskedDB, hLen)
    seedMask=MGF1(maskedDB,hLen)

    #Step h Let maskedSeed = seed \xor seedMask

    maskedSeed = xor_two_str(seed,seedMask)

    #Step i Concatenate a single octet with hexadecimal value 0x00,maskedSeed, and maskedDB EM = 0x00 || maskedSeed || maskedDB.
    EM='00'+maskedSeed+maskedDB
    return EM

M = 'fd5507e917ecbe833878'
seed = '1e652ec152d0bfcd65190ffc604c0933d0423381'
OAEP_encode(M,seed)

def OAEP_decode(EM):
    k=128
    hLen=20
    #Step a
    sha=hl.new("sha1")
    sha.update(bytes.fromhex(''))
    lHash=sha.hexdigest()
    # Step b eparate the encoded message EM into
    # a single octet Y,
    # an octet string maskedSeed of length hLen,
    # and an octet string maskedDB of length k - hLen - 1 as
    # EM = Y || maskedSeed || maskedDB.
    Y=EM[:(len(EM)-hLen*2-k-hLen*2-1*2)]
    maskedSeed = EM[hLen:len(EM)-k-hLen*2-1*2]

    maskedDB=EM[len(Y)+hLen*2:len(EM)]

    # Step c  Let seedMask = MGF(maskedDB, hLen).
    seedMask=MGF1(maskedDB,hLen)

    # Step d Let seed = maskedSeed \xor seedMask
    seed = xor_two_str(maskedSeed,seedMask)

    # Step e Let dbMask = MGF(seed, k - hLen - 1).

    dbMask=MGF1(seed,k-hLen-1)

    # Step f Let DB = maskedDB \xor dbMask.

    DB=xor_two_str(maskedDB,dbMask)

    #Step G ??
    return DB[hLen*2:]

OAEP_decode('0000255975c743f5f11ab5e450825d93b52a160aeef9d3778a18b7aa067f90b2178406fa1e1bf77f03f86629dd5607d11b9961707736c2d16e7c668b367890bc6ef1745396404ba7832b1cdfb0388ef601947fc0aff1fd2dcd279dabde9b10bfc51f40e13fb29ed5101dbcb044e6232e6371935c8347286db25c9ee20351ee82')
